cmake_minimum_required(VERSION 2.8)
project(litl)

include_directories(src)

set (INSTALL_DIR ${CMAKE_BINARY_DIR})
set (LIBRARY_OUTPUT_PATH  ${CMAKE_BINARY_DIR}/lib)
set (EXECUTABLE_OUTPUT_PATH  ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_C_FLAGS "-DVERSION=\\\"0.1.2\\\"")

file(
  GLOB_RECURSE
  source_files
  src/*.c
)

add_library(
  litl
  SHARED
  ${source_files}
)

target_link_libraries(
 litl
  pthread
)

add_executable(
 litl_merge
 utils/litl_merge.c
)

target_link_libraries(
 litl_merge
 litl
)

add_executable(
  litl_print
  utils/litl_print.c
)

target_link_libraries(
  litl_print
  litl
)

add_executable(
  litl_split
  utils/litl_split.c
)

target_link_libraries(
  litl_split
  litl
)

install(FILES
  ${CMAKE_CURRENT_SOURCE_DIR}/src/litl_types.h
  ${CMAKE_CURRENT_SOURCE_DIR}/src/litl_read.h
  ${CMAKE_CURRENT_SOURCE_DIR}/src/litl_write.h
  DESTINATION ${CMAKE_INSTALL_PREFIX}/include
  )

install(TARGETS litl_split litl_print litl_merge litl
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib)