#ifndef EZT_DEMANGLE_H
#define EZT_DEMANGLE_H

/* demangle a string into a newly allocated buffer.
 */
const char* ezt_demangle(const char* mangled_str);

/* initialize demangling. Return 1 if demangling is available, or 0 otherwise */
int ezt_demangle_init();

#endif	/* EZT_DEMANGLE_H */
