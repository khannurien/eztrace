/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef EZTRACE_LITL_PACKED_H
#define EZTRACE_LITL_PACKED_H

#include "eztrace.h"

#include "litl_types.h"
#include "litl_write.h"
#include "litl_read.h"

#define CHECK_RETVAL_PACKED(f)						\
  do {									\
    void* retval = NULL;						\
    if(__ezt_trace.status == ezt_trace_status_running ||		\
       __ezt_trace.status == ezt_trace_status_being_finalized ||	\
       __ezt_trace.status == ezt_trace_status_paused		     ) { \
      f;								\
      if(! retval){							\
	fprintf(stderr, "[EZTrace] The buffer for recording events is full. Stop recording. The trace will be truncated\n");\
	__ezt_trace.status =  ezt_trace_status_stopped;			\
      }									\
    }									\
  } while(0)

#define EZTRACE_EVENT0_PACKED_UNPROTECTED(code) do {			\
    EZT_PRINTF(5, "EZTRACE_EVENT0_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_0(__ezt_trace.litl_trace, code, retval));	\
  } while(0)

#define EZTRACE_EVENT1_PACKED_UNPROTECTED(code, arg1) do {		\
    EZT_PRINTF(5, "EZTRACE_EVENT1_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_1(__ezt_trace.litl_trace, code, arg1, retval)); \
  }while(0)

#define EZTRACE_EVENT2_PACKED_UNPROTECTED(code, arg1, arg2) do {	\
    EZT_PRINTF(5, "EZTRACE_EVENT2_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_2(__ezt_trace.litl_trace, code, arg1, arg2, retval)); \
  } while(0)

#define EZTRACE_EVENT3_PACKED_UNPROTECTED(code, arg1, arg2, arg3) do {	\
    EZT_PRINTF(5, "EZTRACE_EVENT3_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_3(__ezt_trace.litl_trace, code, arg1, arg2, arg3, retval)); \
  } while(0)


#define EZTRACE_EVENT4_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4) do { \
    EZT_PRINTF(5, "EZTRACE_EVENT4_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_4(__ezt_trace.litl_trace, code, arg1, arg2, arg3, arg4, retval));	\
  } while(0)

#define EZTRACE_EVENT5_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5) do { \
    EZT_PRINTF(5, "EZTRACE_EVENT5_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_5(__ezt_trace.litl_trace, code, arg1, arg2, arg3, arg4, arg5, retval)); \
  } while(0)

#define EZTRACE_EVENT6_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6) do { \
    EZT_PRINTF(5, "EZTRACE_EVENT6_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_6(__ezt_trace.litl_trace, code, arg1, arg2, arg3, arg4, arg5, arg6, retval)); \
  } while(0)

#define EZTRACE_EVENT7_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7) do { \
    EZT_PRINTF(5, "EZTRACE_EVENT7_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_7(__ezt_trace.litl_trace, code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, retval)); \
  } while(0)

#define EZTRACE_EVENT8_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) do { \
      EZT_PRINTF(5, "EZTRACE_EVENT8_PACKED(code=%x)\n", code);		\
      CHECK_RETVAL_PACKED(litl_write_probe_pack_8(__ezt_trace.litl_trace, code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, retval)); \
    } while(0)

#define EZTRACE_EVENT9_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9) do { \
    EZT_PRINTF(5, "EZTRACE_EVENT9_PACKED(code=%x)\n", code);		\
    CHECK_RETVAL_PACKED(litl_write_probe_pack_9(__ezt_trace.litl_trace, code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, retval)); \
  } while(0)


#define GET_PARAM_PACKED_1(p_ev, arg1) do {		    \
    litl_read_get_param_1(p_ev, arg1);			    \
  } while(0)

#define GET_PARAM_PACKED_2(p_ev, arg1, arg2) do {	    \
    litl_read_get_param_2(p_ev, arg1, arg2);		    \
  } while(0)

#define GET_PARAM_PACKED_3(p_ev, arg1, arg2, arg3) do {	\
    litl_read_get_param_3(p_ev, arg1, arg2, arg3);	    \
  } while(0)

#define GET_PARAM_PACKED_4(p_ev, arg1, arg2, arg3, arg4) do {	\
    litl_read_get_param_4(p_ev, arg1, arg2, arg3, arg4);	    \
  } while(0)

#define GET_PARAM_PACKED_5(p_ev, arg1, arg2, arg3, arg4, arg5) do {	\
    litl_read_get_param_5(p_ev, arg1, arg2, arg3, arg4, arg5);		\
  } while(0)

#define GET_PARAM_PACKED_6(p_ev, arg1, arg2, arg3, arg4, arg5, arg6) do { \
    litl_read_get_param_6(p_ev, arg1, arg2, arg3, arg4, arg5, arg6);	  \
  } while(0)

#define GET_PARAM_PACKED_7(p_ev, arg1, arg2, arg3, arg4, arg5, arg6, arg7) do { \
    litl_read_get_param_7(p_ev, arg1, arg2, arg3, arg4, arg5, arg6, arg7);      \
  } while(0)

#define GET_PARAM_PACKED_8(p_ev, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) do { \
    litl_read_get_param_8(p_ev, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);      \
  } while(0)

#define GET_PARAM_PACKED_9(p_ev, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9) do { \
    litl_read_get_param_9(p_ev, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);      \
  } while(0)


#define EZTRACE_EVENT_FORCE_RECORD_PACKED_0(code)		\
  {						\
    EZTRACE_PROTECT {				\
      EZTRACE_PROTECT_ON();			\
      EZTRACE_EVENT0_PACKED_UNPROTECTED(code);	\
      EZTRACE_PROTECT_OFF();			\
    }						\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_1(code, arg1)		\
  {							\
    EZTRACE_PROTECT {					\
      EZTRACE_PROTECT_ON();				\
      EZTRACE_EVENT1_PACKED_UNPROTECTED(code, arg1);	\
      EZTRACE_PROTECT_OFF();				\
    }							\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_2(code, arg1, arg2)		\
  {								\
    EZTRACE_PROTECT {						\
      EZTRACE_PROTECT_ON();					\
      EZTRACE_EVENT2_PACKED_UNPROTECTED(code, arg1, arg2);	\
      EZTRACE_PROTECT_OFF();					\
    }								\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_3(code, arg1, arg2, arg3)			\
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT3_PACKED_UNPROTECTED(code, arg1, arg2, arg3);	\
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_4(code, arg1, arg2, arg3, arg4)		\
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT4_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4);	\
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_5(code, arg1, arg2, arg3, arg4, arg5)	\
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT5_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5); \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_6(code, arg1, arg2, arg3, arg4, arg5, arg6) \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT6_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6); \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_7(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7) \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT7_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7); \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_8(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT8_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8); \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_FORCE_RECORD_PACKED_9(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9) \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      EZTRACE_EVENT9_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9); \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }


#define EZTRACE_EVENT_PACKED_0(code)			\
  {						\
    EZTRACE_PROTECT {				\
      EZTRACE_PROTECT_ON();			\
	    if (__ezt_trace.status != ezt_trace_status_paused) {	\
      EZTRACE_EVENT0_PACKED_UNPROTECTED(code);		\
	    } \
      EZTRACE_PROTECT_OFF();			\
    }						\
  }

#define EZTRACE_EVENT_PACKED_1(code, arg1)		\
  {						\
    EZTRACE_PROTECT {				\
      EZTRACE_PROTECT_ON();			\
      if (__ezt_trace.status != ezt_trace_status_paused) {	\
	EZTRACE_EVENT1_PACKED_UNPROTECTED(code, arg1);			\
      }								\
      EZTRACE_PROTECT_OFF();					\
    }						\
  }

#define EZTRACE_EVENT_PACKED_2(code, arg1, arg2)		\
  {							\
    EZTRACE_PROTECT {					\
      EZTRACE_PROTECT_ON();				\
      if (__ezt_trace.status != ezt_trace_status_paused) {	\
      EZTRACE_EVENT2_PACKED_UNPROTECTED(code, arg1, arg2);	\
      } \
      EZTRACE_PROTECT_OFF();				\
    }							\
  }

#define EZTRACE_EVENT_PACKED_3(code, arg1, arg2, arg3)			\
  {								\
    EZTRACE_PROTECT {						\
      EZTRACE_PROTECT_ON();					\
      if (__ezt_trace.status != ezt_trace_status_paused) {	\
      EZTRACE_EVENT3_PACKED_UNPROTECTED(code, arg1, arg2, arg3);	\
      }								\
      EZTRACE_PROTECT_OFF();					\
    }								\
  }

#define EZTRACE_EVENT_PACKED_4(code, arg1, arg2, arg3, arg4)		\
  {								\
    EZTRACE_PROTECT {						\
      EZTRACE_PROTECT_ON();					\
if (__ezt_trace.status != ezt_trace_status_paused) {			\
	EZTRACE_EVENT4_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4);	\
      }									\
      EZTRACE_PROTECT_OFF();					\
    }								\
  }

#define EZTRACE_EVENT_PACKED_5(code, arg1, arg2, arg3, arg4, arg5)		\
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      if (__ezt_trace.status != ezt_trace_status_paused) {	\
      EZTRACE_EVENT5_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5);	\
      } \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_PACKED_6(code, arg1, arg2, arg3, arg4, arg5, arg6)	\
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      if (__ezt_trace.status != ezt_trace_status_paused) {	\
	EZTRACE_EVENT6_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6); \
      }									\
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_PACKED_7(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7)  \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      if (__ezt_trace.status != ezt_trace_status_paused) {		\
      EZTRACE_EVENT7_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7); \
      } \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_PACKED_8(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      if (__ezt_trace.status != ezt_trace_status_paused) {		\
      EZTRACE_EVENT8_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);	\
	    } \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }

#define EZTRACE_EVENT_PACKED_9(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9) \
  {									\
    EZTRACE_PROTECT {							\
      EZTRACE_PROTECT_ON();						\
      if (__ezt_trace.status != ezt_trace_status_paused) {			\
      EZTRACE_EVENT9_PACKED_UNPROTECTED(code, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9); \
	    } \
      EZTRACE_PROTECT_OFF();						\
    }									\
  }


#endif	/* EZTRACE_LITL_PACKED_H */
