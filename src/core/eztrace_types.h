/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#ifndef EZTRACE_TYPES_H
#define EZTRACE_TYPES_H

/* This header contains the data types that are used by both eztrace and
 * eztrace_convert/eztrace_stats
 */

/* application pointer. This pointer is invalid in the current
 * process, but its value corresponds to an object in the
 * application.
 */
typedef intptr_t app_ptr;


#endif/* EZTRACE_TYPES_H */
