#include "ezt_submodule.h"
#include "ezt_getcpu.h"

static int __ezt_nb_submodules = 1;
static struct ezt_submodule __ezt_submodules[] = {
  {
    .exec_init            = ezt_getcpu_init,
    .convert_init         = NULL,
    .convert_handle       = NULL,
    .convert_handle_stats = NULL,
    .convert_print_stats  = NULL
  }
};


void ezt_submodule_exec_init() {
  static int initialized = 0;
  if(initialized)
    return;
  initialized = 1;

  int i;
  for(i=0;i<__ezt_nb_submodules; i++) {
    __ezt_submodules[i].exec_init();
  }
}

static void __ezt_submodule_init(void) __attribute__ ((constructor));
static void __ezt_submodule_init(void) {
  ezt_submodule_exec_init();
}
