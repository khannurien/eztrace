/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

#ifdef USE_MPI3
static void MPI_Ibarrier_prolog (MPI_Comm c, MPI_Fint *r)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(c, &size);
  libMPI_Comm_rank(c, &rank);
  EZTRACE_EVENT_PACKED_4 (EZTRACE_MPI_IBARRIER, (app_ptr)c, rank, size, (app_ptr)r);
}

static int MPI_Ibarrier_core (MPI_Comm c, MPI_Request *r)
{
  return libMPI_Ibarrier(c, r);
}

static void MPI_Ibarrier_epilog (MPI_Comm c, MPI_Fint *r)
{
  EZTRACE_EVENT_PACKED_1 (EZTRACE_MPI_STOP_IBARRIER, (app_ptr)r);
}

int MPI_Ibarrier (MPI_Comm c, MPI_Request *req)
{
  FUNCTION_ENTRY;
  MPI_Ibarrier_prolog(c, (MPI_Fint*)req);
  int ret = MPI_Ibarrier_core(c, req);
  MPI_Ibarrier_epilog(c, (MPI_Fint*)req);

  return ret;
}

void mpif_ibarrier_(MPI_Fint *c, MPI_Fint *r, int *error)
{
  FUNCTION_ENTRY;
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Request c_req = MPI_Request_f2c(*r);

  MPI_Ibarrier_prolog(c_comm, r);
  *error = MPI_Ibarrier_core(c_comm, &c_req);
  *r= MPI_Request_c2f(c_req);
  MPI_Ibarrier_epilog(c_comm, r);
}
#endif
