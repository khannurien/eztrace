/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Universite Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

static void MPI_Bcast_prolog(void *buffer __attribute__((unused)), int count,
                             MPI_Datatype datatype, int root, MPI_Comm comm) {
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int ssize=0;
  if(datatype != MPI_DATATYPE_NULL)
   _EZT_MPI_Type_size(datatype, &ssize);

  int data_size = count * ssize;
  EZTRACE_EVENT_PACKED_5(EZTRACE_MPI_START_BCast, (app_ptr)comm, size, rank, root, data_size);
}

static int MPI_Bcast_core(void *buffer, int count, MPI_Datatype datatype,
                          int root, MPI_Comm comm) {
  return libMPI_Bcast(buffer, count, datatype, root, comm);
}

static void MPI_Bcast_epilog(void *buffer __attribute__((unused)),
			     int count __attribute__((unused)),
			     MPI_Datatype datatype,
			     int __attribute__((unused)) root,
			     MPI_Comm comm)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  EZTRACE_EVENT_PACKED_3(EZTRACE_MPI_STOP_BCast, (app_ptr)comm, size, rank);
}

int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root,
              MPI_Comm comm) {
  FUNCTION_ENTRY;
  MPI_Bcast_prolog(buffer, count, datatype, root, comm);
  int ret = MPI_Bcast_core(buffer, count, datatype, root, comm);
  MPI_Bcast_epilog(buffer, count, datatype, root, comm);
  return ret;
}

void mpif_bcast_(void *buf, int *count, MPI_Fint *d, int *root, MPI_Fint *c,
                 int *error) {
  FUNCTION_ENTRY;
  MPI_Datatype c_type = MPI_Type_f2c(*d);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Bcast_prolog(buf, *count, c_type, *root, c_comm);
  *error = MPI_Bcast_core(buf, *count, c_type, *root, c_comm);
  MPI_Bcast_epilog(buf, *count, c_type, *root, c_comm);
}
