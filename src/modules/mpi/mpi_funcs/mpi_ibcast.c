/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

#ifdef USE_MPI3
static void MPI_Ibcast_prolog(void *buffer __attribute__((unused)),
			      int count __attribute__((unused)),
			      MPI_Datatype datatype,
			      int root,
			      MPI_Comm comm,
			      MPI_Request *r)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);
  int ssize=0;
  if(datatype != MPI_DATATYPE_NULL)
   _EZT_MPI_Type_size(datatype, &ssize);

  int data_size = count * ssize;
  EZTRACE_EVENT_PACKED_6(EZTRACE_MPI_IBCAST, (app_ptr)comm, size, rank, (app_ptr)r, data_size, root);
}

static int MPI_Ibcast_core(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm, MPI_Request *r)
{
  return libMPI_Ibcast(buffer, count, datatype, root, comm, r);
}

static void MPI_Ibcast_epilog(void *buffer __attribute__((unused)),
			      int count __attribute__((unused)),
			      MPI_Datatype datatype __attribute__((unused)),
			      int root __attribute__((unused)),
			      MPI_Comm comm __attribute__((unused)),
			      MPI_Request *r) {
  EZTRACE_EVENT_PACKED_1(EZTRACE_MPI_STOP_IBCAST, (app_ptr)r);
}

int MPI_Ibcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm, MPI_Request *r)
{
  FUNCTION_ENTRY;
  MPI_Ibcast_prolog(buffer, count, datatype, root, comm, r);
  int ret = MPI_Ibcast_core(buffer, count, datatype, root, comm, r);
  MPI_Ibcast_epilog(buffer, count, datatype, root, comm, r);
  return ret;
}

void mpif_ibcast_(void *buf, int *count, MPI_Fint *d,
		 int *root, MPI_Fint *c, MPI_Fint*r, int *error)
{
  FUNCTION_ENTRY;
  MPI_Datatype c_type = MPI_Type_f2c(*d);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Request c_req = MPI_Request_f2c(*r);

  MPI_Ibcast_prolog(buf, *count, c_type, *root, c_comm, r);
  *error = MPI_Ibcast_core(buf, *count, c_type, *root, c_comm, &c_req);
  *r= MPI_Request_c2f(c_req);
  MPI_Ibcast_epilog(buf, *count, c_type, *root, c_comm, r);
}
#endif
