/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright © CNRS, INRIA, Université Bordeaux 1, Telecom SudParis
 * See COPYING in top-level directory.
 */

#define _REENTRANT

#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/timeb.h>
#include <dlfcn.h>
#include <string.h>

#include "mpi.h"
#include "mpi_eztrace.h"
#include "mpi_ev_codes.h"
#include "eztrace.h"

#ifdef USE_MPI3
static void MPI_Ireduce_prolog (CONST void *sendbuf __attribute__((unused)),
				void *recvbuf __attribute__((unused)),
				int count __attribute__((unused)),
				MPI_Datatype datatype,
				MPI_Op op __attribute__((unused)),
				int root,
				MPI_Comm comm,
				MPI_Request *r)
{
  int rank = -1;
  int size = -1;
  libMPI_Comm_size(comm, &size);
  libMPI_Comm_rank(comm, &rank);

  int ssize;
 _EZT_MPI_Type_size(datatype, &ssize);
  int data_size = ssize * count;

  EZTRACE_EVENT_PACKED_6(EZTRACE_MPI_IREDUCE, (app_ptr)comm, size, rank, (app_ptr)r, data_size, root);
}

static int MPI_Ireduce_core (CONST void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype,
			     MPI_Op op, int root, MPI_Comm comm,
			     MPI_Request *r)
{
  return libMPI_Ireduce (sendbuf, recvbuf, count, datatype, op, root, comm, r);
}

static void MPI_Ireduce_epilog (CONST void *sendbuf __attribute__((unused)),
				void *recvbuf __attribute__((unused)),
				int count __attribute__((unused)),
				MPI_Datatype datatype __attribute__((unused)),
				MPI_Op op __attribute__((unused)),
				int root __attribute__((unused)),
				MPI_Comm comm __attribute__((unused)),
				MPI_Request *r) {
  EZTRACE_EVENT_PACKED_1(EZTRACE_MPI_STOP_IREDUCE, (app_ptr)r);
}


int MPI_Ireduce (CONST void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype,
		 MPI_Op op, int root, MPI_Comm comm, MPI_Request *r)
{
  FUNCTION_ENTRY;

  MPI_Ireduce_prolog (sendbuf, recvbuf, count, datatype, op, root, comm, r);
  int ret = MPI_Ireduce_core (sendbuf, recvbuf, count, datatype, op, root, comm, r);
  MPI_Ireduce_epilog (sendbuf, recvbuf, count, datatype, op, root, comm, r);
  return ret;
}


void mpif_ireduce_(void *sbuf, void *rbuf, int *count,
		  MPI_Fint *d, MPI_Fint *op, int *root,
		   MPI_Fint *c, MPI_Fint *r, int *error)
{
  FUNCTION_ENTRY;
  MPI_Datatype c_type = MPI_Type_f2c(*d);
  MPI_Op c_op = MPI_Op_f2c(*op);
  MPI_Comm c_comm = MPI_Comm_f2c(*c);
  MPI_Request c_req = MPI_Request_f2c(*r);

  MPI_Ireduce_prolog(sbuf, rbuf, *count, c_type, c_op, *root, c_comm, r);
  *error = MPI_Ireduce_core(sbuf, rbuf, *count, c_type, c_op, *root, c_comm, &c_req);
  *r = MPI_Request_c2f(c_req);
  MPI_Ireduce_epilog(sbuf, rbuf, *count, c_type, c_op, *root, c_comm, r);
}
#endif
