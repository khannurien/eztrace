# Create : libeztrace-convert-stdio.so
set(CMAKE_C_FLAGS ${C_FLAGS})

set(CMAKE_REQUIRED_INCLUDES "${STARPU_INCLUDE_DIRS}")
foreach(libdir ${STARPU_LIBRARY_DIRS})
  list(APPEND CMAKE_REQUIRED_FLAGS "-L${libdir}")
endforeach()
string(REPLACE ";" " " CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS}")
set(CMAKE_REQUIRED_LIBRARIES "${STARPU_LIBRARIES}")
check_function_exists(starpu_mpi_data_register_comm STARPU_MPI_DATA_REGISTER_COMM_FOUND)
if ( STARPU_MPI_DATA_REGISTER_COMM_FOUND )
  add_definitions(-DHAVE_STARPU_MPI_DATA_REGISTER_COMM)
endif()


# Include directories
include_directories(
  ../../core
  ../../pptrace
  ${CMAKE_BINARY_DIR}/include/
  ${STARPU_INCLUDE_DIRS}
  ${MPI_INCLUDE_PATH}
)

# Libraries diretory
link_directories(${CMAKE_BINARY_DIR}/lib/
  ${STARPU_LIBRARY_DIRS}
  ${CMAKE_BINARY_DIR}/extlib/lib/)

add_library(
  eztrace-convert-starpu
  SHARED
  eztrace_convert_starpu.c
)

target_link_libraries(
 eztrace-convert-starpu
 gtg
)

# Create : libeztrace-starpu.so
add_library(
  eztrace-starpu
  SHARED
  starpu.c
)

target_link_libraries(
  eztrace-starpu
  dl
  litl
  eztrace
  ${STARPU_LIBRARIES}
)

# Create : libeztrace-autostart-stio.so
add_library(
  eztrace-autostart-starpu
  SHARED
  starpu.c
)

target_link_libraries(
  eztrace-autostart-starpu
  dl
  litl
  eztrace
  ${STARPU_LIBRARIES}
)

set_target_properties(eztrace-autostart-starpu PROPERTIES COMPILE_FLAGS "-DEZTRACE_AUTOSTART")
set_target_properties (eztrace-autostart-starpu eztrace-starpu PROPERTIES LINK_FLAGS "-Wl,--no-undefined -Wl,-Bsymbolic")
install(TARGETS eztrace-convert-starpu eztrace-starpu eztrace-autostart-starpu LIBRARY DESTINATION lib)