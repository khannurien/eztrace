/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * isize.h -- determining instruction size
 *
 * Provided method:
 * 	- get_overridden_size(bin, child, symbol, trampoline_size): determines
 * 		how much of *symbol* should be copied to replace the part overridden by
 * 		the trampoline of size *trampoline_size*. *bin* is the opaque structure
 * 		of the binary and *child* the pid of the child process.
 *
 *  Created on: 2 juil. 2011
 *      Author: Damien Martin-Guillerez <damien.martin-guillerez@inria.fr>
 */

#ifndef PPTRACE_ISIZE_H_
#define PPTRACE_ISIZE_H_
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>

ssize_t get_overridden_size(void* bin, pid_t child, word_uint symbol,
                            size_t trampoline_size);

#endif /* PPTRACE_ISIZE_H_ */
