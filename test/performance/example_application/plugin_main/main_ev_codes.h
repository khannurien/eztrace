/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef __main_EV_CODES_H__
#define __main_EV_CODES_H__

/* This file defines the event codes that are used by the example
 * module.
 */
#include "ev_codes.h"

/* 7-bits event codes prefix. This identifies the module and thus should be
 * unique.
 * The 0x0? prefix is reserved for eztrace internal use. Thus you can
 * use any prefix between 0x80 and 0xff.
 */
#define main_EVENTS_ID    USER_MODULE_ID(0x99)
#define main_PREFIX       (main_EVENTS_ID << NB_BITS_EVENTS)

/* Define various event codes used by the example module
 * The 2 most significant bytes should correspond to the module id,
 * as below:
 */
#define EZTRACE_lib_function_1 (main_PREFIX | 0x1)
#define EZTRACE_lib_function_2 (main_PREFIX | 0x2)
#define EZTRACE_bin_function_1 (main_PREFIX | 0x3)
#define EZTRACE_bin_function_2 (main_PREFIX | 0x4)


#endif	/* __main_EV_CODES_H__ */
