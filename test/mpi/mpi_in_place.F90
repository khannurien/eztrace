program test_mpi_in_place
  implicit none
#include "mpif.h"

  integer ierr, rank, size

  call MPI_Init(ierr)

  call MPI_Comm_rank(MPI_COMM_WORLD, rank, ierr)
  call MPI_Comm_size(MPI_COMM_WORLD, size, ierr)

  call MPI_AllReduce(mpi_in_place, rank, 1, MPI_INTEGER, &
       MPI_MAX, MPI_COMM_WORLD, ierr)

  if(rank .ne. size - 1) then
     print *, "Max rank is:", rank
     print *, "It should be:", size
     call abort
  endif

  print *, "Max rank is:", rank

  call MPI_Finalize(ierr)
end program test_mpi_in_place
