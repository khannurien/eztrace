The EZTrace team is pleased to announce the release 1.0.

EZTrace is a tool that aims at generating automatically execution traces from
parallel applications. Without any modification in the application, eztrace can
provide precise information on the program behavior (MPI communications, OpenMP
parallel regions, CUDA kernels, etc.) These informations are gathered in Paje or
OTF trace files that can be visualized with tools such as ViTE.

EZTrace 1.0 is avalaible under the CeCILL-B license and it can be downloaded
from the eztrace website: http://eztrace.gforge.inria.fr/

The following is an abbreviated list of changes in 1.0:

* Add support for CUDA applications
* Add a script that generates an EZTrace plugin directly from an executable
  program
* EZTrace now relies on LiTL (instead of FxT) for recording events
* EZTrace can now track the CPU on which a thread run
* Add a sampling interface that allows to call a function every x ms

--
The EZTrace team
